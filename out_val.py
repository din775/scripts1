import jira
from jira import JIRA
import re
import datetime
import BeautifulTime
from pprint import pprint

options = {
    'server': 'https://sd.mutantbr.com'
}

jira = JIRA(options, basic_auth=(
    'user', 'pass'))

today = BeautifulTime.datetime2str(datetime.datetime.now(), '%d-%m-%Y')
issuesval = {}

for issue in jira.search_issues('status in (Novo, "Em andamento") AND assignee in ("Alarmes Tratados Internamente Mutant") AND summary !~ log ORDER BY created ASC', maxResults=100):
    
    issuesval.update({issue.key:[]})

    for link in issue.fields.issuelinks:

            if hasattr(link, "outwardIssue"):
                outwardIssue = link.outwardIssue
                issuesval[issue.key].append(outwardIssue.fields.status.name)

                                
            if hasattr(link, "inwardIssue"):
                inwardIssue = link.inwardIssue
                issuesval[issue.key].append(inwardIssue.fields.status.name)


for i, issues in issuesval.items():
   output = ""
   for issue in issues:
        if issue == "Em andamento":
            output += ' Chamado em andamento, não mexa'
        elif issue == 'Novo':
            output += ' Galera vagabundeando'
        elif issue == 'Reaberto':
            output += " Chamado Reaberto"
        elif issue == 'Resolvido':
            output += " Chamado resolvido -------------AGIR"
        elif issue == 'Fechado':
            output += " Chamado encerrado ---------AGIR"
        elif issue == 'Agendado':
                output += " Chamado agendado"
        elif issue == 'Pendente':
                output += " Chamado agendado"
        else:
            output += ' Estou perdido'
        print (i + output)
                    
    #if today != last_updated:
    #else:
       # print("O chamado: " + issue.key + " Já está atualizado, aproveite o seu dia NDesk")