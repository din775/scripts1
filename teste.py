import jira
from jira import JIRA
import re
import datetime
import BeautifulTime
import pprint
import dateutil.parser

options = {
    'server': 'https://sd.mutantbr.com'
}

jira = JIRA(options, basic_auth=(
    'user', 'pass'))

today = BeautifulTime.datetime2str(datetime.datetime.now(), '%d-%m-%Y')

for issue in jira.search_issues('status in (Novo, "Em andamento") AND assignee in ("Alarmes Tratados Internamente Mutant") AND summary !~ log ORDER BY created ASC', maxResults=200):

    oissue_comment = jira.comments(issue.key)
    olast_comment = jira.comment(issue.key, oissue_comment[-1])
    oupdated_time = dateutil.parser.parse(olast_comment.updated)
    oupdated_time = oupdated_time.strftime('%d-%m-%Y')
    last_updated = dateutil.parser.parse(issue.fields.updated)
    last_updated = last_updated.strftime('%d-%m-%Y')
    
    if today != last_updated:
 
        for link in issue.fields.issuelinks:

                if hasattr(link, "outwardIssue"):
                    outwardIssue = link.outwardIssue
                    
                    if outwardIssue.fields.status.name != 'Fechado' and outwardIssue.fields.status.name != 'Resolvido':
                        print('\n' + issue.key + ' Esta linkado ao chamado: \n')
                        print(outwardIssue.key + '\n')
                        outissue_comment = jira.comments(outwardIssue.key)
                        if outissue_comment != [] and oissue_comment != []:
                            print(olast_comment.body)
                            outlast_comment = jira.comment(
                                outwardIssue.key, outissue_comment[-1])
                            try:
                                out_updated_comment = dateutil.parser.parse(
                                    outlast_comment.updated)
                            except:
                                print(
                                    'Opss, não consegui converter o horário, comunique a "equipe" de desenvolvimento')
                            finally:
                                out_updated_comment = out_updated_comment.strftime(
                                    '%d-%m-%Y')
                                if out_updated_comment == today:
                                    print('O chamado está atualizado')
                                else:
                                    
                                    print('Atualize o chamado com a seguinte anotação:' + '\t' +
                                        outlast_comment.body + '\n___________________________________________________')
                                    
                if hasattr(link, "inwardIssue"):
                    inwardIssue = link.inwardIssue
                    
                    if inwardIssue.fields.status.name != 'Fechado' and inwardIssue.fields.status.name != 'Resolvido':
                        print('\n' + issue.key + ' Esta linkado ao chamado: \n')
                        print(inwardIssue.key + '\n')
                        inissue_comment = jira.comments(inwardIssue.key)

                        if inissue_comment != [] and oissue_comment != []:
                            print(olast_comment.body)
                            inlast_comment = jira.comment(
                                inwardIssue.key, inissue_comment[-1])
                            try:
                                in_updated_comment = dateutil.parser.parse(
                                    inlast_comment.updated)
                            except:
                                print(
                                    'Opss, não consegui converter o horário, comunique a "equipe" de desenvolvimento')
                            finally:
                                in_updated_comment = in_updated_comment.strftime(
                                    '%d-%m-%Y')
                                if in_updated_comment == today:
                                    print('O chamado está atualizado')
                                else:
                                    print('Atulize o chamado com a seguinte anotação:' + '\t' +
                                        inlast_comment.body + '\n___________________________________________________')
    else:
        print("O chamado: " + issue.key + " Já está atualizado, aproveite o seu dia NDesk")